const Products = require("../models/productsModel.js");
const auth = require("../auth.js");


// Create Product (admin)
module.exports.createProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
		let newProduct = new Products(request.body)
		newProduct.save()
		.then(save => response.send(/*"Product successfully created!"*/true))
		.catch(error => response.send(/*error*/false));
	}else{
		return response.send(/*`account ${userData.email} is not admin`*/false);
	}
}

// Retrieve all products
module.exports.allProducts = (request, response) => {
	Products.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// Retrieve active products
module.exports.activeProducts = (request, response) => {
	Products.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

// Retrieve product(byName or ProductId)
module.exports.findProduct = (request, response) => {
	const productName = request.body.productName;
	const productId = request.body.productId;
	if(productId!==undefined && productName!==undefined){
		Products.find({productId: productId, productName: productName})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}else if(productId!==undefined && productName==undefined){
		Products.find({productId: productId})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}else{
		Products.find({productName: productName})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
	
}

// Retrieve single product(findById)
module.exports.product = (request, response) => {
	const productId = request.params.productId;

	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(/*`Invalid ID`*/false));
}

// Update product information(admin)
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let updatedProduct = {
		productId: request.body.productId,
		productName: request.body.productName,
		description: request.body.description,
		isActive: request.body.isActive,
		price: request.body.price,
		createdOn: new Date()
	}
	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updatedProduct)
		.then(result => response.send(/*`successfully updated`*/true))
		.catch(error => response.send(/*error*/false));
	}else{
		return response.send(/*`You don't have access in this route!`*/false)
	}
}

// activate or archive product(admin)
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let updatedProduct = ({isActive: request.body.isActive});
	if(userData.isAdmin){
		if(updatedProduct == true){
			Products.findByIdAndUpdate(productId, updatedProduct)
			.then(result => response.send(`product is now unarchive`))
			.catch(error => response.send(error));
		}else{
			Products.findByIdAndUpdate(productId, updatedProduct)
			.then(result => response.send(`product is now archive`))
			.catch(error => response.send(error));
		}
			
	}else{
		return response.send(`You don't have access in this route!`)
	}
}
