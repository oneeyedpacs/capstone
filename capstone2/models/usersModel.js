const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, "email is required!"]
		},
		password: {
			type: String,
			required: [true, "password is required!"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		firstName: {
			type: String,
			required: [true, "First name is required!"]
		},
		lastName: {
			type: String,
			required: [true, "Last name is required!"]
		},
		birthDate: {
			type: String,
			required: [true, "birth date is required!"]
		}

})

const Users = mongoose.model("User", userSchema);

module.exports = Users;