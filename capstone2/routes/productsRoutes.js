const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productsControllers = require("../controllers/productsControllers.js");

// Create Product (admin)
router.post("/createProduct", auth.verify, productsControllers.createProduct);
// Retrieve all products
router.get("/allProducts", productsControllers.allProducts);
// Retrieve active products
router.get("/activeProducts", productsControllers.activeProducts);
// Retrieve product(byName or ProductId)
router.get("/findProduct", productsControllers.findProduct)
// Retrieve single product(findById)
router.get("/:productId", productsControllers.product);
// Update product information(admin)
router.patch("/updateProduct/:productId", auth.verify, productsControllers.updateProduct);
// activate or archive product(admin)
router.patch("/archiveProduct/:productId", auth.verify, productsControllers.archiveProduct);

module.exports = router;