import {Container, Row, Col, Button} from 'react-bootstrap';
import Swal2 from 'sweetalert2';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate, Navigate} from 'react-router-dom';

export default function CreateProduct(){

	const navigate = useNavigate();
	const [productId, setProductId] = useState('');
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		if (productId && productName && description && price){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [productId, productName, description, price]);

	function createProduct(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct/`,{
			method: 'POST',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`,
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				description: description,
				price: price
				
			})
		})

		.then(result => result.json())
		.then(data => {
		if(data === false){
				Swal2.fire({
				    title: 'Create failed!',
				    icon: 'error',
				    text: 'Check your product details try again!'
				})
			}else{
				
				Swal2.fire({
				    title : 'Create Successful',
				    icon : 'success',
				    text: 'Create Product Successful!'
				    })
				navigate('/product-catalog');


			}
		})
	}


	return(

		<Container className='my-5'>
			<Row>
			<h1 className = 'text-center my-3'>Create Product</h1>
				<Col className='mx-auto col-12'>
				<form onSubmit = {event => createProduct(event)}>
				      <InputGroup className="mb-3">
				        <InputGroup.Text>Product Name</InputGroup.Text>
				        <Form.Control
				          placeholder="Title"
				          type="text" 
				          value={productName}
				          onChange = {event => {
				          	setProductName(event.target.value)
				          }}
				        />
				      </InputGroup>

				      <InputGroup className="mb-3">
				      	<InputGroup.Text>Product Id</InputGroup.Text>
				        <Form.Control
				          placeholder="ex. ns000"
				          type="text" 
				          value={productId}
				          onChange = {event => {
				          	setProductId(event.target.value)
				          }}
				        /> 
				      </InputGroup>

				      <InputGroup className="mb-3">
				        <InputGroup.Text>Price</InputGroup.Text>
				        <Form.Control 
				        placeholder="enter price"
				        type="number" 
				        value={price}
				        onChange = {event => {
				        	setPrice(event.target.value)
				        }}
				        />
				      </InputGroup>

				      <InputGroup>
				        <InputGroup.Text>Description</InputGroup.Text>
				        <Form.Control 
				        as="textarea"
				        placeholder="product description"
				        type="text" 
				        value={description}
				        onChange = {event => {
				        	setDescription(event.target.value)
				        }}
				        />
				      </InputGroup>

				      <Button
				      	className = "my-3"
					      variant="primary" 
					      type="submit"
					      disabled={isDisabled}>CREATE PRODUCT
					  </Button>

				</form>
				</Col>
			</Row>
		</Container>	
		
		)
}