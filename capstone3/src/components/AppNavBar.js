import {useContext} from 'react';
import UserContext from '../UserContext.js';
import {Button,Container,Form,Nav,Navbar,NavDropdown} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useState, useEffect} from 'react';

export default function AppNavBar(){
const {user} = useContext(UserContext);

	return(
	<Navbar bg="warning" expand="lg">
	  <Container fluid>
	    <Navbar.Brand as = {Link} to = '/'><strong className="title">何でも</strong> Anything</Navbar.Brand>
	    <Navbar.Toggle aria-controls="navbarScroll" />
	    <Navbar.Collapse id="navbarScroll">
	      <Nav
	        className="me-auto my-2 my-lg-0"
	        style={{ maxHeight: '100px' }}
	        navbarScroll
	      >
	        <Nav.Link as = {Link} to = '/'>Home</Nav.Link>
	        <Nav.Link as = {Link} to = 'product-catalog'>Products</Nav.Link>
	        <NavDropdown title="User" id="navbarScrollingDropdown"> 
	        {localStorage.token == null ?
	        <>
	        <NavDropdown.Item as = {Link} to = 'register'>Register</NavDropdown.Item>
	        <NavDropdown.Item as = {Link} to = 'login'>Login</NavDropdown.Item>
	        </> :
	        localStorage.isAdmin == "true" ?
	        <>
	        <NavDropdown.Item as = {Link} to = 'admin-dashboard'>Admin Dashboard</NavDropdown.Item>
	        <NavDropdown.Divider />
	        <NavDropdown.Item as = {Link} to = '/logout'>Logout</NavDropdown.Item>
	        </> :
	        <>
	        <NavDropdown.Item as = {Link} to = 'user-cart'>Cart</NavDropdown.Item>

	        <NavDropdown.Item as = {Link} to = 'user-orders'>Order History</NavDropdown.Item>
	        <NavDropdown.Divider />
	        
	        <NavDropdown.Item as = {Link} to = '/logout'>Logout</NavDropdown.Item>
	        </>

	      }
	        </NavDropdown>
	      </Nav>

	    </Navbar.Collapse>
	  </Container>
	</Navbar>
		)
}