import UserContext from '../UserContext.js';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';

export default function Product(props){

	const {user} = useContext(UserContext);
	const{_id, productName, productId, description, isActive, price} = props.productProp;

	let available = isActive;
	
	if(available){
		available = "available"
	}else{
		available = "archive"
	}


	return(

		<Container className = "mt-5 col-12">
		<Row>
			<Col className = "col-12 d-inline">
			<Card className = "cardHighlight">
			     <Card.Body>
			       <Card.Title>{productName}</Card.Title>
			       <Card.Subtitle>Description:</Card.Subtitle>
			       <Card.Text>{description}</Card.Text>
			       <Card.Subtitle>Available:</Card.Subtitle>
			       <Card.Text>{available}</Card.Text>
			       <Card.Subtitle>Price:</Card.Subtitle>
			       <Card.Text>{price}</Card.Text>
			       <Card.Subtitle>productId:</Card.Subtitle>
			       <Card.Text>{productId}</Card.Text>


			       {
			       	localStorage.isAdmin
			       	?
			       	<Button as = {Link} to = {`/products/${_id}`}>Update Product</Button> 
			       	:
			       	user.id !== null 
			       	?
			       	<>
			       	<Button as = {Link} to = {`/single-products/${_id}`}>Product Details</Button> 
			       	<Button className="ms-2"as = {Link} to = {`/user-Cart/${_id}`}>add to cart</Button> 
			       	</>
			       	:
			       	<Button as = {Link} to = {`/login`}>Add to cart</Button>

			       }
			     </Card.Body>
			   </Card>
			</Col>

		</Row>
		</Container>
		
		)
}
