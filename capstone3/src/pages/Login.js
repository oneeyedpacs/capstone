import UserContext from '../UserContext.js';
import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Link, useNavigate, Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import Swal2 from 'sweetalert2';
import {UserProvider} from '../UserContext.js';


export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);

	const login = (event) => {
		event.preventDefault()


		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method:'POST',
			headers:{
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
			    email: email,
			    password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
				    title: 'Login failed!',
				    icon: 'error',
				    text: 'Check your email/password and try again!'
				})
				navigate('/login');
			}else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(data.auth);
				Swal2.fire({
				    title: 'Login successful!',
				    icon: 'success',
				    text: 'Welcome to nandemo Anything online shop!'
				})



			}
			
		})
		.catch(error => console.log(error))
	}
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: 'GET',
			headers: {
			    Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {

            setUser({
                id : data._id,
                isAdmin: data.isAdmin
            })
            if(data.isAdmin){
            	localStorage.setItem('isAdmin', data.isAdmin);
            	navigate('/admin-dashboard')
            }else{
				navigate('/');
            }
        })

	}

	useEffect(() => {
		if(email && password){
			setIsDisabled(false);
		}else {
			setIsDisabled(true);
		}
	},[email,password])


	return(
		<UserProvider value = {{user, setUser}}>
		
		
		<Container className='my-5'>
		<Row>
		<Col className='mx-auto col-6'>
			<Form onSubmit={login}>
			<h2 className='text-center my-3'>Login</h2>

		      <Form.Group className="mb-3" controlId="email">
		        <Form.Label>Email</Form.Label>
		        <Form.Control 
		        type="email" 
		        placeholder="Enter your Email" 
		        value={email}
		        onChange={event => setEmail(event.target.value)}
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        type="password" 
		        placeholder="Enter your password" 
		        value={password}
		        onChange={event => setPassword(event.target.value)}
		        />
		      </Form.Group>

		      <p>Don't have an account? <Link to = '/register'>Register now</Link></p>

		      <Button 
		      variant="primary" 
		      type="submit"
		      disabled={isDisabled}
		      >LOGIN</Button>

		    </Form>

		</Col>
		</Row>
		</Container>
		</UserProvider>
		)
}