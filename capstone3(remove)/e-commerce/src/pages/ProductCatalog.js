// import productListData from '../data/productList.js';
import Product from '../components/Product.js';
import {Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect} from 'react';

export default function ProductCatalog(){

	const [products, setProducts] = useState([]);
	if(localStorage.isAdmin){

		useEffect(()=>{
			fetch(`http://localhost:4004/products/allProducts`)
			.then(result => result.json())
			.then(data => {

				 setProducts(data.map(product =>{
				 	console.log(product);
				 	return(
				 		<Product key = {product._id} productProp = {product}/>
				 		)
				 }))

			})
		}
		, [])
	}else{
		useEffect(()=>{
			fetch(`http://localhost:4004/products/activeProducts`)
			.then(result => result.json())
			.then(data => {
				 setProducts(data.map(product =>{
				 	return(
				 		<Product key = {product._id} productProp = {product}/>
				 		)
				 }))

			})
		}
		, [])
	}

	return(
		<>
		<h1 className = 'text-center my-3'>Product Catalog</h1>
		{products}
		</>
		)
}