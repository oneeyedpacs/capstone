import {Container, Row, Col} from 'react-bootstrap';

import Welcome from '../components/Welcome.js';
import Trending from '../components/Trending.js';

export default function Home(){
	
	return(
		<>
			<Welcome/>
			<Trending/>

		</>

		)
}